# Longest Common Subsequence

## Description

Given `v` an array with `N` elements we call a subsequence of length `K` of the array `v` a new array `v' = (v[i1], v[i2], ... v[iK])`, where `i1 < i2 < ... < iK`. 
For example, the array `v = (5 7 8 9 1 6)` contains the subsequences `(5 8 6)` or `(7 8 1)`,  but not `(1 5)`. 

Given two arrays `A` and `B` of lengths `M` and `N` find the longest common subsequence.

## Constraints

* 1 ≤ M, N ≤ 1024
* The arrays do not contain numbers greater than 256

## Example

```
For the arrays:
A = (1 7 3 9 8)
B = (7 5 8)

The solution is:
LCS = (7 8)
```

## Indications

* Backtracking can be used to solve the problem, but has an exponential runtime complexity.
* A dynamic programming solution has a runtime complexity of `O(N * M)` and an explanation can be found on [wikipedia](https://en.wikipedia.org/wiki/Longest_common_subsequence_problem).