# Euclid

## Description

Given a pair of integers `(a, b)`, compute the greatest common divisor for each pair. An array of `T` pairs will be solved for each test.

## Constraints

* 1 ≤ T ≤ 100 000
* 2 ≤ a, b ≤ 2 * 10^9
* Execution time < 0.25s

## Indications

* You can find a short presentation of the topic [here](https://en.wikipedia.org/wiki/Greatest_common_divisor).
* The greatest common divisor of two numbers a and b can be found iteratively by going over all numbers between 2 and min(a,b). This solution should only pass 3 tests. 
* To improve the execution time we can use Euclid's algorithm by subtraction which should only pass 6 tests.
* To further improve the runtime we can use Euclid's algoorithm by division which should pass all tests.