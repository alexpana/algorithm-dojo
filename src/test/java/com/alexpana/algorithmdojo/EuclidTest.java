package com.alexpana.algorithmdojo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

@SuppressWarnings("ConstantConditions")
@RunWith(Parameterized.class)
public class EuclidTest {

    @Parameterized.Parameter
    public String testFileName;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"grader_test1"},
                {"grader_test2"},
                {"grader_test3"},
                {"grader_test4"},
                {"grader_test5"},
                {"grader_test6"},
                {"grader_test7"},
                {"grader_test8"},
                {"grader_test9"},
                {"grader_test10"},
        });
    }

    @Test
    public void runTest() {
        int[][] pairs = readInput(testFileName);

        int[] solution = new int[pairs.length];

        long start = System.currentTimeMillis();
        for (int i = 0; i < pairs.length; i++) {
            solution[i] = Euclid.solve(pairs[i][0], pairs[i][1]);
        }
        long end = System.currentTimeMillis();

        Assert.assertArrayEquals(readOutput(testFileName), solution);
        Assert.assertTrue("Time limit exceeded", end - start < 250);
    }

    private int[][] readInput(String testFileName) {
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("euclid/" + testFileName + ".in"));
        int pairCount = scanner.nextInt();
        int[][] pairs = new int[pairCount][2];

        for (int i = 0; i < pairCount; i++) {
            pairs[i][0] = scanner.nextInt();
            pairs[i][1] = scanner.nextInt();
        }
        return pairs;
    }

    private int[] readOutput(String testFileName) {
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("euclid/" + testFileName + ".ok"));
        List<Integer> result = new ArrayList<>();
        while (scanner.hasNext()) {
            result.add(scanner.nextInt());
        }

        int[] output = new int[result.size()];
        int index = 0;
        for (Integer integer : result) {
            output[index++] = integer;
        }
        return output;
    }
}