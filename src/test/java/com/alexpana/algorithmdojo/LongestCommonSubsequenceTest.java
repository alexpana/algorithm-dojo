package com.alexpana.algorithmdojo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

@SuppressWarnings("ConstantConditions")
@RunWith(Parameterized.class)
public class LongestCommonSubsequenceTest {

    private static final int EXECUTION_TIME_MS = 100;

    @Parameterized.Parameter
    public String testFileName;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"grader_test1"},
                {"grader_test2"},
                {"grader_test3"},
                {"grader_test4"},
                {"grader_test5"},
                {"grader_test6"},
                {"grader_test7"},
                {"grader_test8"},
                {"grader_test9"},
                {"grader_test10"},
        });
    }

    @Test
    public void runTest() {
        Input input = readInput(testFileName);


        long start = System.currentTimeMillis();
        int[] solution = LongestCommonSubsequence.solve(input.first, input.second);
        long end = System.currentTimeMillis();

        Assert.assertArrayEquals(readOutput(testFileName), solution);
        Assert.assertTrue("Time limit exceeded", end - start < EXECUTION_TIME_MS);
    }

    private Input readInput(String testFileName) {
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("longestcommonsubsequence/" + testFileName + ".in"));
        int firstCount = scanner.nextInt();
        int secondCount = scanner.nextInt();

        Input input = new Input();
        input.first = new int[firstCount];
        input.second = new int[secondCount];

        for (int i = 0; i < firstCount; i++) {
            input.first[i] = scanner.nextInt();
        }

        for (int i = 0; i < secondCount; i++) {
            input.second[i] = scanner.nextInt();
        }

        return input;
    }

    private int[] readOutput(String testFileName) {
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("longestcommonsubsequence/" + testFileName + ".ok"));
        int count = scanner.nextInt();
        int[] output = new int[count];

        int index = 0;
        while (scanner.hasNext()) {
            output[index++] = scanner.nextInt();
        }
        return output;
    }

    class Input {
        int[] first;
        int[] second;
    }

}