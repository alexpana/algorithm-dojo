# Algorithm Dojo

## About

These problems were adapted from a romanian computer science olympiad training website called [infoarena](https://infoarena.ro/arhiva-educationala). They are meant as educational practice for high school students before they move on to more challenging problems.

## How

Each problem has an empty class in the sources folder, a test harness in the test folder and a description in the resources folder. Solutions for each problem can be found on the `solutions` branch.

After implementing the solution, run the associated tests to see how you scored.

## Why

The infoarena website is invaluable to romanian comp sci students, but it has some disadvantages:

1. The website, problems and forum are exclusively in romanian. 
2. The evaluator requires users to submit the source code in a supported language which is then compiled and executed on their servers.
3. The time restrictions are meant to filter out inefficient solutions but their evaluator uses the same constraints for both compiled languages and Java. Although Java is slower, this should't affect the evaluation of the **algorithm**.
4. Each problem requires you to read and write to a file. The choice of IO API also affects the runtime performance of the program which means students often lose points on an otherwise accepted solution because of their choice of IO API.
5. Most problems (excluding those adapted here) don't provide the user with the inputs for the failed test cases, leaving students in the dark as to what corner case they might be missing.

This repository fixes these problems by:

1. Providing the problem requirements in english.
2. Offering an 'enterprise' testing environment by providing unit tests for each problem.
3. Doesn't require the solution to read or write to files, but instead requires the user to implement the most basic method that is enough to test the solution.
4. Execution times have been adapted to suit the JVM.
